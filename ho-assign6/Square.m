// ******************************************************************************
//
//  Square.m
//  ho-assign6
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-14.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement:
// Create an NSMutableArray in the run method of your Driver  and carry out
// the following tasks:
// 1) Create and store 10 random graphics objects of different type in the
// NSMutable array ( each time you run the program a different set of objects
// will be stored in the NSMutable array). Additionally, include among the
// random objects 2 or 3 random NSString objects.
// 2)Create a loop that goes through the entire array and draws each object by
// using a variable of id data type. Perform a test whether the
// idGraphicsObject contains the draw method if not, do not draw
// (it could be the NSString object).
//
// Inputs:   none
// Outputs:  10 random graphics objects and 2 random NSString objects.
//
// ******************************************************************************

#import "Square.h"

@implementation Square

@synthesize side;

- (void) draw
{
  // Top of square
  for (int count1 = side; count1 > 0; count1--)
  {
    printf("*");
  }

  printf("\n");

  // Middle of square
  for (int count2 = side - 2; count2 > 0; count2--)
  {
    printf("*");
    for (int count3 = side - 2; count3 > 0; count3--)
    {
      printf(" ");
    }
    printf("*\n");
  }

  // Bottom of square
  for (int count4 = side; count4 > 0; count4--)
  {
    printf("*");
  }

  printf("\n");
  printf("\n");
}
@end
