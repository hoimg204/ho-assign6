// ******************************************************************************
//
//  Driver.m
//  ho-assign6
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-14.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement:
// Create an NSMutableArray in the run method of your Driver  and carry out
// the following tasks:
// 1) Create and store 10 random graphics objects of different type in the
// NSMutable array ( each time you run the program a different set of objects
// will be stored in the NSMutable array). Additionally, include among the
// random objects 2 or 3 random NSString objects.
// 2)Create a loop that goes through the entire array and draws each object by
// using a variable of id data type. Perform a test whether the
// idGraphicsObject contains the draw method if not, do not draw
// (it could be the NSString object).
//
// Inputs:   none
// Outputs:  10 random graphics objects and 2 random NSString objects.
//
// ******************************************************************************

#import "Driver.h"
#import "Circle.h"
#import "Ellipse.h"
#import "Driver.h"
#import "Rectangle.h"
#import "Square.h"
#import "XYPoint.h"
#import "Line.h"

@implementation Driver

- (void) run
{
  // Setting a circle object
  Circle * circle = [[Circle alloc] init];

  circle.radius = 15;

  // Setting an ellipse object
  Ellipse * ellipse = [[Ellipse alloc] init];
  ellipse.aRadius = 48;
  ellipse.bRadius = 10;

  // Setting a rectangle object
  Rectangle * rectangle = [[Rectangle alloc] init];
  [rectangle setWidth:2 andHeight:4];

  // Setting a square object
  Square * square = [[Square alloc] init];
  square.side = 4;

  // Setting the points
  XYPoint * startPoint = [XYPoint new];
  XYPoint * endPoint = [XYPoint new];
  [startPoint setX:2 andY:2];
  [endPoint setX:6 andY:2];

  // Setting a line object
  Line * line = [[Line alloc] init];
  [line startPoint:startPoint];
  [line endPoint:endPoint];

  // Setting a string object
  NSString * string = @"You found a string and there isn't draw method :)";

  // Setting a mutable array
  NSMutableArray * array = [[ NSMutableArray alloc ] init];

  // Setting 10 random graphics objects
  for (int count = 0; count < 10; count++)
  {
    int random = arc4random() % 5;

    if (random == 0)
    {
      [array addObject:circle];
    }
    else if (random == 1)
    {
      [array addObject:ellipse];
    }
    else if (random == 2)
    {
      [array addObject:rectangle];
    }
    else if (random == 3)
    {
      [array addObject:square];
    }
    else if (random == 4)
    {
      [array addObject:line];
    }
  }

  // Setting 2 random string objects
  [array addObject:string];
  [array addObject:string];

  // Array size
  const long SIZE = (unsigned long) [array count];

  // Printing the array size
  NSLog(@"Array Size = %lu\n\n", SIZE);

  // Loop to show all array
  for (int count = 0; count < SIZE; count++)
  {
    // id variable
    id idGraphicsObject = array[count];

    // test whether the idGraphicsObject contains the draw method
    if ([idGraphicsObject isKindOfClass:[NSString class]])
    {
      NSLog(@"%@", idGraphicsObject);
    }
    else
    {
      [idGraphicsObject draw];
    }
  }

}

@end