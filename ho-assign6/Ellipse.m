// ******************************************************************************
//
//  Ellipse.m
//  ho-assign6
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-14.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement:
// Create an NSMutableArray in the run method of your Driver  and carry out
// the following tasks:
// 1) Create and store 10 random graphics objects of different type in the
// NSMutable array ( each time you run the program a different set of objects
// will be stored in the NSMutable array). Additionally, include among the
// random objects 2 or 3 random NSString objects.
// 2)Create a loop that goes through the entire array and draws each object by
// using a variable of id data type. Perform a test whether the
// idGraphicsObject contains the draw method if not, do not draw
// (it could be the NSString object).
//
// Inputs:   none
// Outputs:  10 random graphics objects and 2 random NSString objects.
//
// ******************************************************************************

#import "Ellipse.h"

@implementation Ellipse

@synthesize aRadius, bRadius;

- (void) draw
{
  // instance variables needs to be defined and initilized
  char myChar = '*';
  double PI = 3.14159;
  int size = (aRadius > bRadius) ? aRadius : bRadius;
  char array[size + 2][size + 2];

  // store space char in the array
  for (int row = 0; row < size + 2; row++)
  {
    for (int column = 0; column < size + 2; column++)
    {
      array[row][column] = ' ';
    }
  }

  // store myChar where circle exists
  for (double angle = 0; angle < 2 * PI; angle += (2 * PI / 180) )
  {
    int xCoordinate = 1 + (int) (cos(angle) * (aRadius / 2) +  size / 2);
    int yCoordinate = 1 + (int) (sin(angle) * (bRadius / 2) +  size / 2);
    array[xCoordinate][yCoordinate] = myChar;
  }

  // display the array representing circle
  for (int row = 0; row < size + 2; row++)
  {
    for (int column = 0; column < size + 2; column++)
    {
      printf("%c", array[row][column]);
    }
    printf("\n");
  }
  printf("\n");
  printf("\n");
}

@end